<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$Users = DB::table('users');
		$Users->delete();
		$Users->insert([
				'username' => 'demo',
				'password' => Hash::make('demo'),
			]);
	}

}