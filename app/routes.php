<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// App::bind('BookmarkRepositoryInterface','BookmarkMongoRepository');

Route::get('db{type}', ['as' => 'setdb' ,
	function($type){
	Session::put('dbtype',$type);
	return Redirect::route('bookmark.index');
}]);

App::bind('HomeController', function(){
	$dbtype = 'Mongo';
	if(Session::has('dbtype'))
		$dbtype = Session::get('dbtype');
	else
		Session::put('dbtype',$dbtype);
	$RepoType = 'Bookmark'.$dbtype.'Repository';
	return new HomeController(new $RepoType);
});

Route::resource('bookmark', 'HomeController');
Route::get('/','HomeController@index');
Route::get('/bookmark/{id}/delete',[ 'uses'=>'HomeController@destroy','as' => 'bookmark.delete' ]);
Route::get('/bookmark/{id}/rate/{rate}',[ 'uses'=>'HomeController@rate','as' => 'bookmark.rate' ]);