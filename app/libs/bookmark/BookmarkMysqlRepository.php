<?php

class BookmarkMysqlRepository implements BookmarkRepositoryInterface
{
	public function all()
	{
		return BookmarkMysql::all();
	}

	public function find($id)
	{
		return BookmarkMysql::find($id);
	}
	
	public function destroy($id)
	{
		return BookmarkMysql::destroy($id);
	}

	public function create($data)
	{
		$data['rate'] = 0;

		return BookmarkMysql::create($data);
	}

	public function update($id, $data)
	{

		$bookmark = $this->find($id);
		foreach ($data as $key => $value) {
			$bookmark->$key = $value;				
		}

		return $bookmark->save();	
	}

	public function rate($id,$rate)
	{
		$bookmark = $this->find($id);
		$bookmark->rate += $rate;
		return $bookmark->save();
	}

}