<?php

class BookmarkMongoRepository implements BookmarkRepositoryInterface
{
	public function all()
	{
		return BookmarkMongo::all();
	}

	public function find($id)
	{
		return BookmarkMongo::find($id);
	}
	
	public function destroy($id)
	{
		return BookmarkMongo::destroy($id);
	}

	public function create($data)
	{
		$data['rate'] = 0;

		return BookmarkMongo::create($data);
	}

	public function update($id, $data)
	{

		$bookmark = $this->find($id);
		foreach ($data as $key => $value) {
			$bookmark->$key = $value;				
		}

		return $bookmark->save();	
	}

	public function rate($id,$rate)
	{
		$bookmark = $this->find($id);
		$bookmark->rate += $rate;
		return $bookmark->save();
	}

}